#include <stdio.h>
#include "md5.h"
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
char pssd[100];

    FILE *passwords;
    FILE *out;
    
    passwords = fopen(argv[1],"r");
    
    while(fscanf(passwords, "%s", pssd)!= EOF)
    {
        char *hash = md5(pssd, strlen(pssd));
        
        out = fopen(argv[2], "a");
        if (!out)
        {
            printf("Can't open %s for writing\n", argv[2]);
            exit(1);
        }
        fprintf(out, "%s\n", hash);
        fclose(out);
    }
    fclose(passwords);
}
// clang hashpass.c md5.c -o hashpass -l crypto -l ssl
//   ./hashpass rockyou100.txt hello1.txt
